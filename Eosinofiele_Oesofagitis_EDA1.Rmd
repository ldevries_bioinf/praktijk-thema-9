---
title: "Eosinofiele Oesofagitis EDA"
author: "Linda de Vries"
date: '27th September 2020'
output:
  pdf_document: default
  html_document:
    df_print: paged
---
\newpage

\tableofcontents
\newpage

```{r setup, include=F}
knitr::opts_chunk$set(echo = F, message = F, cache = T, warning = F)
```

```{r, message=FALSE}
library(haven)
library(knitr)
library(dplyr)
library(ggplot2)
library(reshape2)
# In terminal wc -l to check if the data set is complete 
# Reading the data 
data <- read_sav("data/eosinofiele_oesofagitis_dataset.sav")
# the data has now 79 obs. of 879 variables

# Removing the NA's in the column
data <- data[, colSums(is.na(data)) != nrow(data)]

# Removing the NA's in the row
data <- data[rowSums(is.na(data)) != ncol(data), ]

# The head of the data
#(kable(data[1:10, 1:8]))
```

# Results
If you look at the histological response then you can descibe the effectness of the treatment. In the data there is a attribute of the neocate which has the diets value in it. There are two diets FFED and FFED + Neocate. And in the histological response are there three; no response, partial response and full response. With a full response there will be often an indicate that the prognosis will be better. 
```{r, out.width='80%', fig.cap="Barplot of the diets FFED and FFED + Neocate, compared to the histological response.", fig.align='center'}
# Compare immune response between standard and Neocate diets
data_resp <- select(data, 
                    2, 
                    Histological_Response)

# Filter and removing the NA in rows
data_resp <- data_resp[rowSums(is.na(data_resp)) != ncol(data_resp),]

data_resp$Neocate <- as.factor(data_resp$Neocate)
data_resp$Histological_Response <- as.factor(data_resp$Histological_Response)
response_count <- count(
  data_resp, 
  Neocate, 
  Histological_Response)

# Creating the barplot of the two diets
ggplot(response_count, aes(x = Neocate, 
                           y = n, 
                           fill = Histological_Response)) + 
  ggtitle("Barplot of the diets FFED and FFED + Neocate") +
  geom_bar(stat = "identity", 
           position = position_dodge()) +
  geom_text(aes(label = n), 
            vjust = 1.6, 
            color = "white",
            position = position_dodge(0.9), 
            size = 3.5) +
  ylab("Amount of cases") +
  scale_fill_discrete(name = "Histological Response", 
                      labels = c("No response", 
                                "Partial response", 
                                "Full response")) +
  scale_x_discrete(labels = c("FFED", "FFED + Neocate"))
```
Well if you look at figure 1, you can see the difference between the two diets. The FFED has five patients that has a full response and the neocate diets has ten patients that has a full response. So it seems like that the neocate diet could be in the protection of patients against
Eosinofiele Oesofagitis.   
\newpage 

The PEC value is a value that measures howmany white bloodcells have accumulated in the tissue. That is another thing to look forward. We can look at how te normalized PEC values are in the baseline and how the values are after six weeks with the diets. 
```{r, out.width='60%', fig.cap="Barplot of PEC baseline compared to Neocate. The mean of the normalized white bloodcells counts.", fig.align='center'}
# Compare levels of peak baseline and after sixweaks
peak_eos <- select(data, 
                   2, 
                   LN_PeakEosBaseline_Max, 
                   LN_PeakEosSixwk_Max)

# Filter and removing the NA in rows
peak_eos <- peak_eos[rowSums(is.na(peak_eos)) != ncol(peak_eos),]

peak_eos$Neocate <- as.factor(peak_eos$Neocate)

# Calculating the mean of the baseline/sixweek neocate (0/1)
mean_LN_PeakEosBaseline_Max_0 <- mean(peak_eos$LN_PeakEosBaseline_Max[peak_eos$Neocate == 0])
mean_LN_PeakEosBaseline_Max_1 <- mean(peak_eos$LN_PeakEosBaseline_Max[peak_eos$Neocate == 1])
mean_LN_PeakEosSixwk_Max_0 <- mean(peak_eos$LN_PeakEosSixwk_Max[peak_eos$Neocate == 0])
mean_LN_PeakEosSixwk_Max_1 <- mean(peak_eos$LN_PeakEosSixwk_Max[peak_eos$Neocate == 1])
mean_baseline <- c(mean_LN_PeakEosBaseline_Max_0, 
                   mean_LN_PeakEosBaseline_Max_1)
mean_sixwk <- c(mean_LN_PeakEosSixwk_Max_0, 
                mean_LN_PeakEosSixwk_Max_1)

neocate <- as.factor(c(0, 1))

# Creating the data frame
mean_data <- data_frame(neocate, mean_baseline, mean_sixwk)

# Plotting the data of PEC mean baseline
ggplot(data = mean_data, aes(x = neocate, 
                             y = mean_baseline, 
                             fill = neocate)) + 
  geom_bar(stat = "identity", 
           position = position_dodge()) +
  geom_text(aes(label = mean_baseline), 
            vjust = 1.6, 
            color = "white",
            position = position_dodge(0.9), 
            size = 3.5) + 
  ggtitle("Mean of the PEC baseline") +
  ylab("Mean baseline") +
  xlab("Neocate") +
  scale_fill_discrete(name = "Neocate", 
                      labels = c("FFED", "FFED + Neocate")) +
  scale_x_discrete(labels = c("FFED", 
                              "FFED + Neocate"))
```
```{r, out.width='60%', fig.cap="Barplot of PEC after sixweeks compared to Neocate. The mean of the normalized white bloodcells counts.", fig.align='center'}
# Plotting the data of PEC mean sixweek
ggplot(data = mean_data, aes(x = neocate, 
                             y = mean_sixwk, 
                             fill = neocate)) + 
  geom_bar(stat = "identity", 
           position = position_dodge()) +
  geom_text(aes(label = mean_sixwk),
            vjust = 1.6, 
            color = "white",
            position = position_dodge(0.9), 
            size = 3.5) +
  ggtitle("Mean of the PEC of six weeks") +
  ylab("Mean after sixweeks") +
  xlab("Neocate") +
  scale_fill_discrete(name = "Neocate") +
  scale_x_discrete(labels = c("FFED", 
                              "FFED + Neocate"))
```
As you can see in figure 2 the mean will be almost the same because it is the baseline. When you take a look to figure 3 there is a difference in the white blood cells counts. The diet FFED + neocate is almost almost halving in size. So what we described in figure 1 confirmed this again that the FFED + neocate a improved histological response. 
\newpage

There will always be a biological difference between males and females. For example, the male metabolism is mostly faster than its female counterpart. In the visualization below we can look at the gender difference when it comes to histological response to the diets.
```{r, out.width='70%', fig.cap="Response of the two diet and compared per gender.", fig.align='center'}
# Compare to immune response severity between gender
data_resp <- select(data, 
                    2, 
                    Histological_Response, 
                    Gender)

# Filter and removing the NA in rows
data_resp <- data_resp[rowSums(is.na(data_resp)) != ncol(data_resp),]

data_resp$Neocate <- as.factor(data_resp$Neocate)
resp_gender <- count(data_resp, Neocate, Histological_Response, Gender)
resp_gender$Gender <- as.factor(resp_gender$Gender)

# Creating the label names for the Neocate
neoct.names <- c("FFED", "FFED and Neocate")
names(neoct.names) <- c(0, 1)

# Ploting the response per diet and gender
ggplot(resp_gender, aes(x = Histological_Response, 
                        y = n, 
                        fill = Gender)) +
  geom_bar(stat="identity", 
           position = "stack") +
  ggtitle("Response per diet and per gender") +
  ylab("Amount of ocurrence") +
  xlab("Response intensity") +
  facet_wrap(~ Neocate, 
             labeller = labeller(Neocate = neoct.names)) +
  scale_fill_discrete(name="Gender",
                      labels =c("Female", "Male")) 
```
Figure 4 clearly shows more men with a full histological response to FFED than women. For FFED + neocate these numbers are equal. For this comparison it is important to note that there were more female (24) than male (16) participants.
\newpage

# Conclusion and discussion 
First of all this dataset has so many NA's so there is so much of missing data. Also are there just 40 patients, so it is not that reliable. Even if we look at the plot with compared to gender, there are more female than male. And even then it was the equal amount. So maybe if there were more men there were a clear difference.  

When we look at the two diets it is very clear that FFED + Neocate shows better results than FFED. 

