Authors: L. de Vries   
Date: 20th November 2020   
Version: Bio-informatica 3, praktijk opdracht 09   
 

# Project name 
Eosinofiele Oesofagitis

# Introduction 
Food allergy is common in Eosinophilic Esophagitis (EoE), and elimination diets can be an effective treatment. In addition to having an allergenic role, nutrition may have immunomodulatory effects. The research question is 'Can you find out by using machine learning what the effect is of the diet four food elimination diet (FFED) with or without neocate on an allergic reaction of the intestines?'.   

# Prerequisites  
The program requires the following packages to be installed:  
- Rstudio_1.2.5019               
- R_3.5.2     
See 'Manual' on how to install these packages.

# Installing
Clone the source files. Type this in the terminal ```git clone https://ldevries_bioinf@bitbucket.org/ldevries_bioinf/praktijk-thema-9.git``` or look at the files and download it on the bitbucket page ```https://bitbucket.org/ldevries_bioinf/praktijk-thema-9/src/master/```.

# Manual 
The folder praktijk-thema-9 contains the files you use to get the results. In the directory from praktijk-thema-9 you can open the ```package_installer.Rmd``` file and start with running, to install all the packages you need. This ```Eosinofiele_Oesofagitis_EDA2.Rmd``` is the file where I logged everything. This can be run after the package installer but it is not necessary because there is a pdf of it. The ```Eosinofiele_Oesofagitis_EDA2.pdf``` file is the pdf file.

# Support 
- For information you can mail to: [Linda's email](ld.de.vries@st.hanze.nl)

# Credits 
- https://michielnoback.github.io/bincourses/data_mining_project.html
- Michiel Noback
- Fenna Feenstra
- Classmates